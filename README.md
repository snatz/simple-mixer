## simple-mixer

_simple-mixer_ is an audio mixer taking as input one or multiple audio tracks (ideally multiple formats supported ) and renders one single audio track (ideally 
multiple audio formats supported as well). Thanks to it, you are able to mix/modify your favorite songs, edit them, add/save samples, overlays tracks, and save 
the result so you can listen to it with your friends.

Features:
* Input of multiple audio tracks.
* Overlays multiple tracks.
* Renders one audio track.
* Different audio qualities available for rendering. (removed - difficult)
* Cut/copy/paste/delete and reorder parts of the songs. (changed with drags & context menu)
* Samples database (such as beats) which can be played as a loop on the background. (useless)
* Selection of a part of a song and save it as a sample for future reuse. (useless)
* Graphical interface.

Bonus features (thoughts, not implemented):
* Record audio from mic/computer.
* Waveform view of tracks (Audacity-like).
* Audio effects.
* Share your song on social medias.
* Save your audio editing project.
* Support more formats.

### Requirements

_ffmpeg_ and _ffprobe_ in the `bin` folder.

### Usage

Step one: Launch `simple-mixer.jar` in a folder containing `bin/ffmpeg.exe` and `bin/ffprobe.exe`.

Step two: Mix your songs!

### Authors

Simon Bar & Édouard François