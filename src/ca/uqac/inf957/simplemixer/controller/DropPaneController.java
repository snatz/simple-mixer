package ca.uqac.inf957.simplemixer.controller;

import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;


public class DropPaneController {
    private Pane dropPane = new BorderPane();
    private TabController parentController;
    private static String DROP_LABEL = "Drop your song here.";

    public DropPaneController(TabController parent)
    {
        parentController = parent;
        Text infoText = new Text(DROP_LABEL);
        infoText.setTextAlignment(TextAlignment.CENTER);
        infoText.setFont(new Font(19));

        dropPane.setOnDragDropped(this::dropDetection);
        dropPane.setOnDragOver(this::draggingVerification);
        dropPane.setOnDragExited(this::removePanelDrag);
        dropPane.setOnDragEntered(this::setPanelDrag);

        ((BorderPane) dropPane).setCenter(infoText);
        dropPane.setPrefHeight(150);
        dropPane.minHeight(150);
        dropPane.setBackground(new Background(new BackgroundFill(Color.BLUEVIOLET, new CornerRadii(1),
                new Insets(0.0,0.0,0.0,0.0))));
    }

    @FXML
    private void draggingVerification(DragEvent event) {
        final Dragboard db = event.getDragboard();

        final boolean isAccepted = db.getFiles().get(0).getName().toLowerCase().endsWith(".mp3");
                //|| db.getFiles().get(0).getName().toLowerCase().endsWith(".flac");

        if (db.hasFiles() && isAccepted) {
            event.acceptTransferModes(TransferMode.ANY);
        } else {
            event.acceptTransferModes(TransferMode.NONE);
        }
        event.consume();
    }

    @FXML
    private void dropDetection(DragEvent event) {
        parentController.tryLoadFile(event);
    }

    @FXML
    private void removePanelDrag(DragEvent event) {
        dropPane.setBackground(new Background(new BackgroundFill(Color.BLUEVIOLET, new CornerRadii(1),
                new Insets(0.0,0.0,0.0,0.0))));
        event.consume();
    }

    @FXML
    private void setPanelDrag(DragEvent event) {
        final Dragboard db = event.getDragboard();

        final boolean isAccepted = db.getFiles().get(0).getName().toLowerCase().endsWith(".mp3");
                //|| db.getFiles().get(0).getName().toLowerCase().endsWith(".flac");

        if (db.hasFiles() && isAccepted) {
            dropPane.setBackground(new Background(new BackgroundFill(Color.DARKSEAGREEN, new CornerRadii(1),
                    new Insets(0.0,0.0,0.0,0.0))));
            event.acceptTransferModes(TransferMode.ANY);
        } else {
            dropPane.setBackground(new Background(new BackgroundFill(Color.INDIANRED, new CornerRadii(1),
                    new Insets(0.0,0.0,0.0,0.0))));
            event.acceptTransferModes(TransferMode.NONE);
        }

        event.consume();
    }

    public Pane getDropPane() {
        return dropPane;
    }
}
