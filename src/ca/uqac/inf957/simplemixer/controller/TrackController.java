package ca.uqac.inf957.simplemixer.controller;

import ca.uqac.inf957.simplemixer.core.AudioEditor;
import ca.uqac.inf957.simplemixer.core.SoundMixer;
import ca.uqac.inf957.simplemixer.model.Track;
import ca.uqac.inf957.simplemixer.model.TrackPart;
import ca.uqac.inf957.simplemixer.utils.DragUtils;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.TitledPane;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class TrackController {
    private Pane pane = new Pane();
    private int trackId;
    private static int MARGIN = 7;
    private static int DEFAULT_RECT_WIDTH = 200;
    private static int RECT_HEIGHT = 100;
    private List<Rectangle> rectanglesList = new ArrayList<>();

    private AccordionController accordionController;
    private TitledPane titledPane;

    public TrackController(Track input, AccordionController aC, TitledPane tP) {

        addTrack(input);
        accordionController = aC;
        titledPane = tP;
        pane.setMaxHeight(RECT_HEIGHT + 10);
        pane.setOnDragEntered(this::setPanelDrag);
        pane.setOnDragOver(this::draggingVerification);
        pane.setOnDragDropped(this::dropDetection);
        pane.setOnDragExited(this::removePanelDrag);
    }
    @FXML
    private void draggingVerification(DragEvent event) {
        final Dragboard db = event.getDragboard();

        final boolean isAccepted = db.getFiles().get(0).getName().toLowerCase().endsWith(".mp3");

        if (db.hasFiles() && isAccepted) {
            event.acceptTransferModes(TransferMode.ANY);
        } else {
            event.acceptTransferModes(TransferMode.NONE);
        }
        event.consume();
    }

    @FXML
    private void setPanelDrag(DragEvent event) {
        final Dragboard db = event.getDragboard();
        final boolean isAccepted = db.getFiles().get(0).getName().toLowerCase().endsWith(".mp3");
        if (db.hasFiles() && isAccepted) {
            pane.setBackground(new Background(new BackgroundFill(Color.DARKSEAGREEN, new CornerRadii(1),
                    new Insets(0.0,0.0,0.0,0.0))));
            event.acceptTransferModes(TransferMode.ANY);
        } else {
            pane.setBackground(new Background(new BackgroundFill(Color.INDIANRED, new CornerRadii(1),
                    new Insets(0.0,0.0,0.0,0.0))));
            event.acceptTransferModes(TransferMode.NONE);
        }
        event.consume();
    }

    @FXML
    private void dropDetection(DragEvent event) {
        event.getDragboard().getFiles().forEach(item -> {
            if (item.getName().endsWith(".mp3")) {
                Duration end = AudioEditor.getSongDuration(item.toPath());
                Duration trackEnd = SoundMixer.getTrack(trackId).getDuration();
                TrackPart tp = new TrackPart(item.toPath(), Duration.ZERO, end, trackEnd);
                addTrackPart(tp);
            }
        });
    }


    @FXML
    private void removePanelDrag(DragEvent event) {
        pane.setBackground(new Background(new BackgroundFill(Color.WHITESMOKE, new CornerRadii(1),
                new Insets(0.0,0.0,0.0,0.0))));
        event.consume();
    }

    private void addTrack(Track input) {
        trackId = input.getTrackId();
        SoundMixer.addTrack(input);
        Rectangle r;
        for (TrackPart tp : input.getTrackParts()) {
            r = DragUtils.createDraggableRectangle(
                    input.getTrackId(),
                    tp.getTrackPartId(),
                    MARGIN,
                    MARGIN + tp.getStartInTrack().getSeconds() / DragUtils.secondsPixelRatio,
                    MARGIN,
                    tp.getDuration().getSeconds() / DragUtils.secondsPixelRatio,
                    RECT_HEIGHT,
                    pane,
                    this
            );
            rectanglesList.add(r);
        }
    }

    public void RemoveRectangle(Rectangle r)
    {
        rectanglesList.remove(r);
        if(rectanglesList.size() <= 0)
        {
            accordionController.deleteEntry(titledPane);
        }
    }
    private void addTrackPart(TrackPart tp) {
        SoundMixer.getTrack(trackId).addPart(tp);
        Rectangle r = DragUtils.createDraggableRectangle(
                trackId,
                tp.getTrackPartId(),
                MARGIN,
                MARGIN + tp.getStartInTrack().getSeconds() / DragUtils.secondsPixelRatio,
                MARGIN,
                tp.getDuration().getSeconds() / DragUtils.secondsPixelRatio,
                RECT_HEIGHT,
                pane,
                this
        );


        rectanglesList.add(r);
    }

    public Pane getPane() {
        return pane;
    }

    public int getTrackId() {
        return trackId;
    }
}
