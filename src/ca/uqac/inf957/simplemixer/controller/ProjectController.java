package ca.uqac.inf957.simplemixer.controller;

import ca.uqac.inf957.simplemixer.core.SoundMixer;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;

import java.io.File;

public class ProjectController {
    private TabController parentController;

    private Pane controlPane = new BorderPane();
    private Button playButton;
    private Button exportButton;

    private static String PLAY_LABEL = "Play";
    private static String STOP_LABEL = "Stop";
    private static String EXPORT_LABEL = "Export as...";

    public ProjectController(TabController parent) {
        parentController = parent;
        playButton = new Button(PLAY_LABEL);
        playButton.setOnAction(e -> clickPlayButton());
        exportButton = new Button(EXPORT_LABEL);
        exportButton.setOnAction(e -> clickExportButton());

        ((BorderPane) controlPane).setCenter(playButton);
        ((BorderPane) controlPane).setRight(exportButton);
    }

    public Pane getPane() {
        return controlPane;
    }

    private void clickPlayButton() {
        if (!parentController.getWindowController().getMusicPlayer().isPlaying()) {
            // Refresh the media.
            parentController.getWindowController().getMusicPlayer().setFilePath(SoundMixer.getTempFile().toPath());
            parentController.getWindowController().getMusicPlayer().play();
            playButton.setText(STOP_LABEL);
        } else {
            parentController.getWindowController().getMusicPlayer().stop();
            playButton.setText(PLAY_LABEL);
        }
    }

    private void clickExportButton() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(EXPORT_LABEL);
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("MP3", "*.mp3"));

        File file = fileChooser.showSaveDialog(parentController.getWindowController().getStage());

        if (file != null) {
            SoundMixer.exportAs(file.getPath());
        } else {
            System.err.println("Selected file is null.");
        }
    }

}
