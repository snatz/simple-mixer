package ca.uqac.inf957.simplemixer.controller;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class DurationBar {
    private int height = 20;
    private Rectangle rectangle;

    public DurationBar()
    {
        rectangle = new Rectangle(0,0, 100, height);
        rectangle.setFill(Color.GREEN);
    }
    public Rectangle getRectangle()
    {
        return rectangle;
    }
}
