package ca.uqac.inf957.simplemixer.controller;

import javafx.event.Event;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.input.DragEvent;
import javafx.scene.layout.BorderPane;

public class TabController {
    private WindowController parentController;
    private AccordionController accordionController;
    private DropPaneController dropPaneController;
    private ProjectController projectController;
    private Tab tab;
    private BorderPane pane = new BorderPane();
    private DurationBar durationBar = new DurationBar();

    public TabController(WindowController parent) {
        parentController = parent;
        accordionController = new AccordionController(this);
        dropPaneController = new DropPaneController(this);
        projectController = new ProjectController(this);
        pane.setBottom(dropPaneController.getDropPane());

        tab = new Tab("New Tab", pane);
        tab.setContent(pane);
        tab.setOnClosed(t -> closePanel());

        tab.setOnSelectionChanged(this::changeTabFocused);
    }
    public void CloseTab()
    {
        parentController.deleteTab(tab);
    }

    private void closePanel() {
        //  save ? if last panel : clear
        parentController.destroyTab(this);
    }

    private void firstDropFiles(String name) {
        pane.setTop(projectController.getPane());

        BorderPane borderPane = new BorderPane();
        ScrollPane scrollPaneVertical = new ScrollPane();
        scrollPaneVertical.setContent(accordionController.getPane());

        borderPane.setTop(scrollPaneVertical);
        //borderPane.setBottom(durationBar.getRectangle());
        ScrollPane scrollPaneHorizontal = new ScrollPane();
        scrollPaneHorizontal.setContent(borderPane);
        pane.setCenter(scrollPaneHorizontal);

        pane.setBottom(dropPaneController.getDropPane());
        tab.setText(name);
        parentController.createNewTab();
    }

    public void tryLoadFile(DragEvent event) {
        event.getDragboard().getFiles().forEach(item -> {
            if (item.getName().endsWith(".mp3")) {
                if (accordionController.getEntries().size() <= 0)
                    firstDropFiles(item.getName());

                accordionController.addEntry(item);
            }
        });
    }

    public Tab getTab() {
        return tab;
    }

    void changeTabFocused(Event event) {

    }

    public WindowController getWindowController() {
        return parentController;
    }
}
