package ca.uqac.inf957.simplemixer.controller;

import ca.uqac.inf957.simplemixer.core.MusicPlayer;
import ca.uqac.inf957.simplemixer.core.SoundMixer;
import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.util.ArrayList;

public class WindowController {
    @FXML
    TabPane tabs;
    @FXML
    StackPane MainPane;

    private Stage stage;

    private ArrayList<TabController> listTabs = new ArrayList<>();
    private MusicPlayer musicPlayer;

    public WindowController() {
        SoundMixer.windowController = this;
        musicPlayer = new MusicPlayer(SoundMixer.getTempFile().toPath());
    }

    public void createNewTab() {
        TabController tab = new TabController(this);
        listTabs.add(tab);
        tabs.getTabs().add(tab.getTab());
    }

    public void deleteTab(Tab t)
    {
        listTabs.remove(t);
        tabs.getTabs().removeAll(t);
    }

    public void showLoadingPanel(String message)
    {
        MainPane.getChildren().removeAll(tabs);

        javafx.scene.text.Text t = new javafx.scene.text.Text();
        t.setFont(Font.font(32));
        t.setText(message);

        MainPane.getChildren().addAll(t);
        t.toFront();
    }

    public void hideLoadingPanel()
    {
        MainPane.getChildren().removeAll(MainPane.getChildren());
        MainPane.getChildren().addAll(tabs);
        tabs.toFront();
    }

    public void destroyTab(TabController tab) {
        listTabs.remove(tab);

        if (listTabs.size() <= 0) {
            createNewTab();
        }
    }

    public MusicPlayer getMusicPlayer() {
        return musicPlayer;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Stage getStage() {
        return stage;
    }
}
