package ca.uqac.inf957.simplemixer.controller;

import ca.uqac.inf957.simplemixer.core.AudioEditor;
import ca.uqac.inf957.simplemixer.model.Track;
import ca.uqac.inf957.simplemixer.model.TrackPart;
import javafx.scene.control.Accordion;
import javafx.scene.control.TitledPane;

import java.io.File;
import java.time.Duration;
import java.util.ArrayList;

public class AccordionController {
    private Accordion pane = new Accordion();

    private ArrayList<TitledPane> entries = new ArrayList<>();
    private TabController parentController;

    public void deleteEntry(TitledPane p) {
        entries.remove(p);
        pane.getPanes().removeAll(p);
        if(entries.size() <= 0)
        {
            parentController.CloseTab();
        }
    }

    public void addEntry(File file) {
        TitledPane tPane = new TitledPane();
        tPane.setText(file.getName());
        tPane.setPrefHeight(150);
        tPane.setPrefWidth(parentController.getWindowController().getStage().getWidth()-25);

        Track t = new Track(new TrackPart(file.toPath(), Duration.ZERO, AudioEditor.getSongDuration(file.toPath())));

        tPane.setContent(new TrackController(t, this, tPane).getPane());
        pane.getPanes().addAll(tPane);
        entries.add(tPane);
    }

    public AccordionController(TabController parent) {
        parentController = parent;
    }

    public Accordion getPane() {
        return pane;
    }

    public ArrayList<TitledPane> getEntries() {
        return entries;
    }
}
