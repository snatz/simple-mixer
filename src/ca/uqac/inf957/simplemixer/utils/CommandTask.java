package ca.uqac.inf957.simplemixer.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CommandTask implements Runnable {
    private String cmd;
    private boolean stdoutputStream = false;

    public CommandTask(String command) {
        cmd = command;
    }

    public CommandTask(String command, boolean stdoutput) {
        cmd = command;
        stdoutputStream = stdoutput;
    }

    @Override
    public void run() {
        try {
            Process pr = Runtime.getRuntime().exec(cmd);
            BufferedReader reader;

            if (!stdoutputStream) {
                reader = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
            } else {
                reader = new BufferedReader(new InputStreamReader(pr.getInputStream()));
            }

            String line;

            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }

            pr.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String getCommand() {
        return cmd;
    }
}
