package ca.uqac.inf957.simplemixer.utils;

import ca.uqac.inf957.simplemixer.controller.TrackController;
import ca.uqac.inf957.simplemixer.core.SoundMixer;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

import java.time.Duration;
import java.util.Arrays;

public final class DragUtils {
    public static final double secondsPixelRatio = 0.6;  // One minute is 100 pixels.

    /**
     * Create a draggable rectangle displaying the current TrackPart.
     * @param trackId The track id.
     * @param trackPartId The track part id.
     * @param margin The margin from the edges of the pane.
     * @param x The x position of the rectangle.
     * @param y The y position of the rectangle.
     * @param width The wisth of the rectangle.
     * @param height The height of the rectangle.
     * @param pane The pane.
     * @param tC The TrackController instance.
     * @return The final Rectangle instance.
     */
    public static Rectangle createDraggableRectangle(int trackId, int trackPartId, int margin,
                                                     double x, double y, double width, double height, Pane pane,
                                                     TrackController tC) {
        final double handleRadius = 5;
        Rectangle rect = new Rectangle(x, y, width, height);
        rect.setFill(Color.LIGHTGRAY);

        // left resize handle:
        Circle resizeHandleWest = new Circle(handleRadius, Color.GRAY);
        resizeHandleWest.centerXProperty().bind(rect.xProperty());
        resizeHandleWest.centerYProperty().bind(rect.yProperty().add(rect.heightProperty().divide(2)));

        // right resize handle:
        Circle resizeHandleEast = new Circle(handleRadius, Color.GRAY);
        // bind to bottom right corner of Rectangle:
        resizeHandleEast.centerXProperty().bind(rect.xProperty().add(rect.widthProperty()));
        resizeHandleEast.centerYProperty().bind(rect.yProperty().add(rect.heightProperty().divide(2)));

        // move handle:
        Circle moveHandle = new Circle(handleRadius, Color.GRAY);
        // bind to bottom center of Rectangle:
        moveHandle.centerXProperty().bind(rect.xProperty().add(rect.widthProperty().divide(2)));
        moveHandle.centerYProperty().bind(rect.yProperty().add(rect.heightProperty().divide(3)));

        pane.getChildren().addAll(rect, moveHandle, resizeHandleEast, resizeHandleWest);

        // force circles to live in same parent as rectangle:
        rect.parentProperty().addListener((obs, oldParent, newParent) -> {
            // Checking remove case
            if (newParent != null) {
                for (Circle c : Arrays.asList(resizeHandleWest, resizeHandleEast, moveHandle)) {
                    Pane currentParent = (Pane) c.getParent();
                    if (currentParent != null) {
                        currentParent.getChildren().remove(c);
                    }
                    ((Pane) newParent).getChildren().add(c);
                }
            }
        });

        Wrapper<Point2D> mouseLocation = new Wrapper<>();

        setUpDragging(resizeHandleWest, mouseLocation);
        setUpDragging(resizeHandleEast, mouseLocation);
        setUpDragging(moveHandle, mouseLocation);

        resizeHandleWest.setOnMouseDragged(event -> {
            if (mouseLocation.value != null) {
                double deltaX = event.getSceneX() - mouseLocation.value.getX();
                double newX = rect.getX() + deltaX;

                if (newX >= handleRadius && newX <= rect.getX() + rect.getWidth() - handleRadius) {
                    rect.setX(newX);
                    rect.setWidth(rect.getWidth() - deltaX);

                    SoundMixer.getTrack(trackId).getTrackPart(trackPartId).setStart(
                            Duration.ofSeconds((long) ((rect.getX() - margin) * secondsPixelRatio)));
                }

                mouseLocation.value = new Point2D(event.getSceneX(), event.getSceneY());
            }
        });

        resizeHandleEast.setOnMouseDragged(event -> {
            if (mouseLocation.value != null) {
                double deltaX = event.getSceneX() - mouseLocation.value.getX();
                double newMaxX = rect.getX() + rect.getWidth() + deltaX;

                if (newMaxX >= rect.getX() && newMaxX <= rect.getParent().getBoundsInLocal().getWidth() - handleRadius) {
                    rect.setWidth(rect.getWidth() + deltaX);

                    SoundMixer.getTrack(trackId).getTrackPart(trackPartId).setEnd(
                            Duration.ofSeconds((long) (rect.getWidth() * secondsPixelRatio)));
                }

                mouseLocation.value = new Point2D(event.getSceneX(), event.getSceneY());
            }
        });

        moveHandle.setOnMouseDragged(event -> {
            if (mouseLocation.value != null) {
                double deltaX = event.getSceneX() - mouseLocation.value.getX();
                double newX = rect.getX() + deltaX ;
                double newMaxX = newX + rect.getWidth();

                if (newX >= handleRadius && newMaxX <= rect.getParent().getBoundsInLocal().getWidth() - handleRadius) {
                    rect.setX(newX);

                    SoundMixer.getTrack(trackId).getTrackPart(trackPartId).setTrackStart(
                            Duration.ofSeconds((long) ((rect.getX() - margin) * secondsPixelRatio)));
                }

                mouseLocation.value = new Point2D(event.getSceneX(), event.getSceneY());
            }

        });

        final ContextMenu contextMenu = new ContextMenu();
        MenuItem delete = new MenuItem("Delete part");

        delete.setOnAction(event -> {
            SoundMixer.getTrack(trackId).removePart(trackPartId);

            if (SoundMixer.getTrack(trackId).getTrackParts().isEmpty()) {
                SoundMixer.removeTrack(trackId);
            }

            pane.getChildren().removeAll(rect, moveHandle, resizeHandleEast, resizeHandleWest);
            tC.RemoveRectangle(rect);
        });
        final ContextMenu infoTextMenu = new ContextMenu();
        MenuItem infoText = new MenuItem();
        infoTextMenu.getItems().add(infoText);
        rect.setOnMousePressed(event -> {
            if (event.isSecondaryButtonDown()) {
                contextMenu.show(rect, event.getScreenX(), event.getScreenY());
            }
            else if(event.isPrimaryButtonDown())
            {
                StringBuilder stringBuilder = new StringBuilder();

                stringBuilder.append("File : ")
                        .append(SoundMixer.getTrack(trackId).getTrackPart(trackPartId).getSongPath().getFileName())
                        .append('\n');
                stringBuilder.append("From : ")
                        .append(parseTime(SoundMixer.getTrack(trackId).getTrackPart(trackPartId).getStartInTrack()))
                        .append('\n');
                stringBuilder.append("To : ")
                        .append(parseTime(SoundMixer.getTrack(trackId).getTrackPart(trackPartId).getEndInTrack()))
                        .append('\n');
                stringBuilder.append("Duration : ")
                        .append(parseTime(SoundMixer.getTrack(trackId).getTrackPart(trackPartId).getDuration()));

                infoText.setText(stringBuilder.toString());
                infoTextMenu.show(rect, event.getScreenX(), event.getScreenY());
            }
            event.consume();
        });


        contextMenu.getItems().add(delete);
        return rect;
    }

    private static String parseTime(Duration dur) {
        long seconds = dur.getSeconds();
        if(seconds > 60)
            return seconds/60 + "min" + seconds % 60 + "sec";

        return seconds + "sec";
    }

    private static void setUpDragging(Circle circle, Wrapper<Point2D> mouseLocation) {
        circle.setOnDragDetected(event -> {
            circle.getParent().setCursor(Cursor.CLOSED_HAND);
            mouseLocation.value = new Point2D(event.getSceneX(), event.getSceneY());
        });

        circle.setOnMouseReleased(event -> {
            circle.getParent().setCursor(Cursor.DEFAULT);
            mouseLocation.value = null;
        });
    }

    private static class Wrapper<T> {
        T value;
    }
}
