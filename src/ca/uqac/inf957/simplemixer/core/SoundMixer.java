package ca.uqac.inf957.simplemixer.core;

import ca.uqac.inf957.simplemixer.controller.WindowController;
import ca.uqac.inf957.simplemixer.model.Track;
import javafx.concurrent.Task;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

/**
 * Static class which define the audio mixer project currently opened.
 */
public final class SoundMixer {
    public static WindowController windowController;
    // List of tracks created.
    private static List<Track> tracks = new ArrayList<>();

    // Temporary file.
    private static File tempFile;

    // Modified flag: if it is on, we have to render before playing or exporting.
    private static boolean modified = false;

    /**
     * Private constructor.
     */

    private SoundMixer() {

    }

    // End of static constructor.
    static {
        try {
            tempFile = File.createTempFile("simplemixer-", ".mp3");
        } catch (IOException e) {
            System.err.println("Could not create a temporary file for the project. Cause: " + e.getMessage());
        }
    }

    /**
     * Add a track to the project.
     * @param track The input track.
     */
    public static void addTrack(Track track) {
        tracks.add(track);

        render();
    }

    /**
     * Remove a track from the project.
     * @param id The id of the track to be removed.
     */
    public static void removeTrack(int id) {
        tracks.remove(getTrack(id));

        render();
    }

    /**
     * Render (overlay) the tracks.
     * @return The ffmpeg worker thread.
     */
    public static void render() {
        windowController.showLoadingPanel("Rendering... ");

        Task task = new Task() {
            @Override
            public Void call() {
            renderCore();
            return null;
            }
        };

        task.setOnSucceeded(e -> windowController.hideLoadingPanel());
        new Thread(task).start();
    }

    private static void renderCore() {
        // Rendering!

        ArrayList<Path> tracksPath = new ArrayList<>();
        ArrayList<Thread> threads = new ArrayList<>();

        for (Track t : tracks) {
            threads.add(t.render());
            tracksPath.add(t.getTempFile().toPath());
        }

        for (Thread t : threads) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        Thread worker = AudioEditor.overlayAudio(tracksPath, tempFile.toPath());

        try {
            worker.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        SoundMixer.modified = false;
    }

    public static void exportAs(String path) {
        if (isModified()) {
            render();
        }

        try {
            Files.copy(tempFile.toPath(), new File(path).toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void modify() {
        if (!isModified()) {
            modified = true;
        }
    }

    public static boolean isModified() {
        return modified;
    }

    /**
     * Retrieve the tracks.
     * @return The tracks of the project.
     */
    public static List<Track> getTracks() {
        return tracks;
    }

    /**
     * Retrieve a specific Track based on its identifier.
     * @param id The id of the Track.
     * @return The Track asked for.
     */
    public static Track getTrack(int id) {
        for (Track t : tracks) {
            if (t.getTrackId() == id) {
                return t;
            }
        }

        return null;
    }

    /**
     * Retrieve the number of tracks the current project has.
     * @return The number of tracks.
     */
    public static int getTrackCount() {
        return tracks.size();
    }

    /**
     * Retrieve the temporary file.
     * @return The temporary File.
     */
    public static File getTempFile() {
        return tempFile;
    }
}
