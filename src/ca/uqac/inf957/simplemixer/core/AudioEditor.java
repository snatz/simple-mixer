package ca.uqac.inf957.simplemixer.core;

import ca.uqac.inf957.simplemixer.utils.CommandTask;
import ca.uqac.inf957.simplemixer.utils.OSUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class AudioEditor {
    public static final String cwd = System.getProperty("user.dir");

    public static final String BIN_PATH = cwd  + "/bin/";
    public static final String OUTPUT_PATH = cwd + "/output/";

    public static final String FFMPEG_WIN_PATH = BIN_PATH + "ffmpeg.exe ";
    public static final String FFMPEG_BASH_PATH = BIN_PATH + "ffmpeg ";

    public static final String FFPROBE_WIN_PATH = BIN_PATH + "ffprobe.exe ";
    public static final String FFPROBE_BASH_PATH = BIN_PATH + "ffprobe ";

    public static final String CMD_DELEGATE_WIN = "cmd /c ";
    public static final String CMD_DELEGATE_BASH = "bash -c ";

    public static boolean overwriteOutput = true;

    private static String getFfmpegCommandInit() {
        if (OSUtils.isWindows()) {
            return CMD_DELEGATE_WIN + FFMPEG_WIN_PATH;
        } else if (OSUtils.isUnix() || OSUtils.isMac()) {
            return CMD_DELEGATE_BASH + FFMPEG_BASH_PATH;
        }

        return "";
    }

    private static String getFfprobeCommandInit() {
        if (OSUtils.isWindows()) {
            return CMD_DELEGATE_WIN + FFPROBE_WIN_PATH;
        } else if (OSUtils.isUnix()) {
            return CMD_DELEGATE_BASH + FFPROBE_BASH_PATH;
        }

        return "";
    }

    /**
     * Compute and retrieve the duration of a song thanks to ffprobe.
     * @param input The song path.
     * @return The duration of the song.
     */
    public static Duration getSongDuration(Path input) {
        StringBuilder sb = new StringBuilder();

        sb.append(getFfprobeCommandInit())
          .append("-v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 ")
          .append(input.toString());

        try {
            Process pr = Runtime.getRuntime().exec(sb.toString());
            BufferedReader reader = new BufferedReader(new InputStreamReader(pr.getInputStream()));
            String line = reader.readLine();

            if (line != null) {
                String d = line.split("\\.")[0];
                return Duration.ofSeconds(Long.valueOf(d));
            } else {
                return Duration.ZERO;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return Duration.ZERO;
        }
    }

    /**
     * Crop an audio file.
     * @param input The input audio file path.
     * @param output The output path.
     * @param from The starting time of the song window to keep.
     * @param to The ending time of the song window to keep.
     * @return The ffmpeg worker thread.
     */
    public static Thread cropAudio(Path input, Path output, Duration from, Duration to) {
        StringBuilder sb = new StringBuilder();

        sb.append(getFfmpegCommandInit());

        if (overwriteOutput) {
            sb.append("-y ");
        }

        sb.append("-ss ").append(from.getSeconds()).append(" ");
        sb.append("-t ").append(to.getSeconds() - from.getSeconds()).append(" ");
        sb.append("-i ").append(input.toString()).append(" ");
        sb.append("-acodec copy ");
        sb.append(output.toString());

        System.out.println("Executing " + sb.toString());

        CommandTask ct = new CommandTask(sb.toString());
        Thread t = new Thread(ct);
        t.start();

        return t;
    }

    /**
     * Delay audio by a given amount of seconds.
     * @param input The input audio file path.
     * @param output The output audio file path.
     * @param delay The delay.
     * @return The ffmpeg worker thread.
     */
    public static Thread delayAudio(Path input, Path output, Duration delay) {
        // Generating silence.
        File tempFile;

        try {
            tempFile = File.createTempFile("silence-", ".mp3");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        Thread silence = createSilence(tempFile.toPath(), delay);

        // Concatenate both the silence and the input file.
        ArrayList<Path> paths = new ArrayList<>();
        paths.add(tempFile.toPath());
        paths.add(input);

        try {
            silence.join();
            return concatenateAudio(paths, output);
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Overlay multiple song inputs.
     * @param inputs The input path List.
     * @param output The output path.
     * @return The ffmpeg worker thread.
     */
    public static Thread overlayAudio(List<Path> inputs, Path output) {
        StringBuilder sb = new StringBuilder();

        if (inputs.size() == 1) {
            sb.append(CMD_DELEGATE_WIN).append("copy ")
                    .append(inputs.get(0).toString()).append(" ")
                    .append(output.toString());
        } else {
            sb.append(getFfmpegCommandInit());

            if (overwriteOutput) {
                sb.append("-y ");
            }

            for (Path input : inputs) {
                sb.append("-i ").append(input.toString()).append(" ");
            }

            sb.append("-filter_complex \"");

            for (int i = 0; i < inputs.size(); i++) {
                sb.append("[").append(i).append(":0]");
                i++;
            }

            sb.append("amix=inputs=").append(inputs.size()).append(":duration=longest\" ");
            sb.append("-c:a libmp3lame ");
            sb.append(output.toString());
        }

        System.out.println("Executing " + sb.toString());

        CommandTask ct = new CommandTask(sb.toString());
        Thread t = new Thread(ct);
        t.start();

        return t;
    }

    /**
     * Concatenate multiple audio files.
     * @param inputs The input path list.
     * @param output The output path.
     * @return The ffmpeg worker thread.
     */
    public static Thread concatenateAudio(List<Path> inputs, Path output) {
        StringBuilder sb = new StringBuilder();

        if (inputs.size() == 1) {
            sb.append(CMD_DELEGATE_WIN).append("copy ")
                    .append(inputs.get(0).toString()).append(" ")
                    .append(output.toString());
        } else {
            sb.append(getFfmpegCommandInit());

            if (overwriteOutput) {
                sb.append("-y ");
            }

            sb.append("-i concat:\"");

            for (int i = 0; i < inputs.size(); i++) {
                sb.append(inputs.get(i).toString());

                if (i < inputs.size() - 1) {
                    sb.append("|");
                }
            }

            sb.append("\" -acodec copy ").append(output.toString());
        }

        System.out.println("Executing " + sb.toString());

        CommandTask ct = new CommandTask(sb.toString());
        Thread t = new Thread(ct);
        t.start();

        return t;
    }

    /**
     * Create a silenced mp3 file.
     * @param output The path to which create the file.
     * @param length The length of the file in seconds.
     * @return The ffmpeg worker thread.
     */
    private static Thread createSilence(Path output, Duration length) {
        StringBuilder sb = new StringBuilder();

        sb.append(getFfmpegCommandInit());

        if (overwriteOutput) {
            sb.append("-y ");
        }

        sb.append("-f lavfi -i aevalsrc=0:0::duration=").append(length.getSeconds()).append(" ");
        sb.append("-ab 320k ");
        sb.append(output.toString());

        System.out.println("Executing " + sb.toString());

        CommandTask ct = new CommandTask(sb.toString());
        Thread t = new Thread(ct);
        t.start();

        return t;
    }
}
