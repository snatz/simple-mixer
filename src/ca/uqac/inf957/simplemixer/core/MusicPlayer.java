package ca.uqac.inf957.simplemixer.core;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.nio.file.Path;

public class MusicPlayer {
    private MediaPlayer mediaPlayer;
    private boolean playing = false;

    public MusicPlayer(Path path) {
        mediaPlayer = new MediaPlayer(new Media(path.toUri().toString()));
    }

    public void play() {
        if (mediaPlayer != null) {
            if (SoundMixer.isModified()) {
                SoundMixer.render();
            } else {
                mediaPlayer.play();
                playing = true;
            }
        }
    }

    public void pause() {
        if (mediaPlayer != null) {
            mediaPlayer.pause();
            playing = false;
        }
    }

    public void stop() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            playing = false;
        }
    }

    public boolean isPlaying() {
        return playing;
    }

    public void setFilePath(Path path) {
        mediaPlayer = new MediaPlayer(new Media(path.toUri().toString()));
    }
}
