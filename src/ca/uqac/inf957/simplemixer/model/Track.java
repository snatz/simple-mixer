package ca.uqac.inf957.simplemixer.model;

import ca.uqac.inf957.simplemixer.core.AudioEditor;
import ca.uqac.inf957.simplemixer.core.SoundMixer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.time.Duration;
import java.util.ArrayList;
import java.util.LinkedList;

public class Track {
    // Given id.
    private static int id = 0;

    // The track identifier.
    private int trackId;

    // The different track parts.
    private LinkedList<TrackPart> parts;

    // Temporary render file per Track
    private File tempFile;

    /**
     * Default constructor.
     */
    public Track() {
        parts = new LinkedList<>();
    }

    public Track(TrackPart firstPart) {
        parts = new LinkedList<>();
        parts.addLast(firstPart);
    }

    /**
     * Track constructor from given track parts.
     * @param trackParts The track parts to be added to the new track.
     */
    public Track(LinkedList<TrackPart> trackParts) {
        parts = trackParts;
    }

    // End of constructors.
    {
        trackId = newId();

        try {
            tempFile = File.createTempFile("track" + trackId + "-", ".mp3");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Add a new part to the track.
     * @param part The part to be added.
     */
    public void addPart(TrackPart part) {
        if (!isPartConflicting(part)) {
            parts.addLast(part);
            SoundMixer.modify();
        }
    }

    /**
     * Add a new part to the track at a given index.
     * @param index The index of the part.
     * @param part The track part.
     */
    public void addPart(int index, TrackPart part) {
        if (!isPartConflicting(part)) {
            parts.add(index, part);
            SoundMixer.modify();
        }
    }

    /**
     * Check if the given part is conflicting with another on the track.
     * @param part The track to check.
     * @return True if it is conflicting, false otherwise.
     */
    public boolean isPartConflicting(TrackPart part) {
        for (TrackPart tp : parts) {
            // If the given part starts somewhere during another part
            // or if it ends somewhere during another part.
            if ((tp.getStart().compareTo(part.getStart()) < 0 && tp.getEnd().compareTo(part.getStart()) > 0) ||
                    (tp.getStart().compareTo(part.getEnd()) < 0 && tp.getEnd().compareTo(part.getEnd()) > 0))
                return true;
        }

        return false;
    }

    /**
     * Remove a part from the track.
     * @param id The identifier of the part to be removed.
     */
    public void removePart(int id) {
        parts.remove(getTrackPart(id));
        SoundMixer.modify();
    }

    /**
     * Render (concatenate) the track part.
     * @return The ffmpeg worker thread.
     */
    public Thread render() {
        ArrayList<Path> partsPath = new ArrayList<>();
        ArrayList<Thread> threads = new ArrayList<>();

        for (TrackPart tp : parts) {
            threads.add(tp.render());
            partsPath.add(tp.getTempFile().toPath());
        }

        for (Thread t : threads) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return AudioEditor.overlayAudio(partsPath, tempFile.toPath());
    }

    /**
     * Generate a new identifier.
     * @return The new identifier.
     */
    private int newId() {
        return id++;
    }

    /**
     * Retrieve the track identifier.
     * @return The track id.
     */
    public int getTrackId() {
        return trackId;
    }

    /**
     * Retrieve the track parts.
     * @return The track parts as a LinkedList.
     */
    public LinkedList<TrackPart> getTrackParts() {
        return parts;
    }

    /**
     * Retrieve selected track part.
     * @param id The identifier.
     * @return The chosen TrackPart.
     */
    public TrackPart getTrackPart(int id) {
        for (TrackPart tp : parts) {
            if (tp.getTrackPartId() == id) {
                return tp;
            }
        }

        return null;
    }

    /**
     * Retrieve the number of parts in the current track.
     * @return The number of parts in the current track.
     */
    public int getTrackPartCount() {
        return parts.size();
    }

    /**
     * Retrieve the track duration.
     * @return The track duration.
     */
    public Duration getDuration() {
        if (parts.size() == 0) {
            return Duration.ZERO;
        } else {
            return parts.getLast().getStartInTrack().plus(parts.getLast().getDuration());
        }
    }

    /**
     * Retrieve the temporary file.
     * @return The temp file.
     */
    public File getTempFile() {
        return tempFile;
    }
}
