package ca.uqac.inf957.simplemixer.model;

import ca.uqac.inf957.simplemixer.core.AudioEditor;
import ca.uqac.inf957.simplemixer.core.SoundMixer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class TrackPart {
    // Given id.
    private static int id = 0;

    // Track part identifier.
    private int trackPartId;

    // The path of the song.
    private Path songPath;

    // The starting time in the track.
    private Duration startAtInTrack;

    // The starting time of the track part.
    private Duration startAt;

    // The ending time of the track part.
    private Duration endAt;

    // Temporary render file per TrackPart
    private File tempFile;

    /**
     * TrackPart contructor.
     * @param songPath The path of the song.
     * @param startAt The starting time of the part.
     * @param endAt The ending time of the part.
     */
    public TrackPart(Path songPath, Duration startAt, Duration endAt) {
        this.songPath = songPath;
        this.startAt = startAt;
        this.endAt = endAt;
        this.trackPartId = newId();
        this.startAtInTrack = Duration.ZERO;

        try {
            tempFile = File.createTempFile("trackpart" + trackPartId + "-", ".mp3");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public TrackPart(Path songPath, Duration startAt, Duration endAt, Duration startAtInTrack) {
        this(songPath, startAt, endAt);

        setTrackStart(startAtInTrack);
    }

    /**
     * Change the song path.
     * @param songPath The song path.
     */
    public void setSongPath(Path songPath) {
        this.songPath = songPath;
        SoundMixer.modify();
    }

    /**
     * Divide the current TrackPart into multiple, smaller TrackParts.
     * @param divisionPoints The division points.
     * @return A List of new TrackPart instances resulting from the division (excluding the current TrackPart, which has
     * its endAt attribute modified).
     */
    public List<TrackPart> divideAt(List<Duration> divisionPoints) {
        List<TrackPart> divisions = new ArrayList<>();
        Duration end = endAt;

        for (int point = 0; point < divisionPoints.size(); point++) {
            if (point == 0) {
                endAt = divisionPoints.get(point);
            }

            if (point >= divisionPoints.size() - 1) {
                divisions.add(new TrackPart(getSongPath(), divisionPoints.get(point), end));
            } else {
                divisions.add(new TrackPart(getSongPath(), divisionPoints.get(point), divisionPoints.get(point + 1)));
            }
        }

        SoundMixer.modify();

        return divisions;
    }

    /**
     * Render (crop and delay) the track part.
     * @return The ffmpeg worker thread.
     */
    public Thread render() {
        if (startAtInTrack.equals(Duration.ZERO)) {
            return AudioEditor.cropAudio(songPath, tempFile.toPath(), startAt, endAt);
        } else {
            File silenceTempFile;

            try {
                silenceTempFile = File.createTempFile("silencetemp" + trackPartId + "-", ".mp3");
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }

            Thread worker = AudioEditor.cropAudio(songPath, silenceTempFile.toPath(), startAt, endAt);

            try {
                worker.join();
                return AudioEditor.delayAudio(silenceTempFile.toPath(), tempFile.toPath(), startAtInTrack);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    /**
     * Generate a new identifier.
     * @return The new identifier.
     */
    private int newId() {
        return id++;
    }

    /**
     * Set the starting time of the current TrackPart.
     * @param d The starting Duration.
     */
    public void setStart(Duration d) {
        if (!d.isNegative() && endAt.minus(d).compareTo(Duration.ZERO) >= 0) {
            startAt = d;
            SoundMixer.modify();
        }
    }

    /**
     * Set the starting time of the current TrackPart, relatively to the Track.
     * @param d The starting Duration.
     */
    public void setTrackStart(Duration d) {
        if (!d.isNegative()) {
            startAtInTrack = d;
            SoundMixer.modify();
        }
    }

    /**
     * Set the ending time of the current TrackPart.
     * @param d The ending Duration.
     */
    public void setEnd(Duration d) {
        if (!d.isNegative() && d.minus(startAt).compareTo(Duration.ZERO) >= 0) {
            endAt = d;
            SoundMixer.modify();
        }
    }

    /**
     * Retrieve the duration of the part.
     * @return The duration of the complete part.
     */
    public Duration getDuration() {
        return endAt.minus(startAt);
    }

    /**
     * Retrieve the starting time of the part.
     * @return The starting time.
     */
    public Duration getStart() {
        return startAt;
    }

    /**
     * Retrieve the starting time of the part in the track.
     * @return The starting time.
     */
    public Duration getStartInTrack() {
        return startAtInTrack;
    }

    /**
     * Retrieve the ending time of the part in the track.
     * @return The ending time.
     */
    public Duration getEndInTrack() {
        return startAtInTrack.plus(getDuration());
    }

    /**
     * Retrieve the ending time of the part.
     * @return The ending time.
     */
    public Duration getEnd() {
        return endAt;
    }

    /**
     * Retrieve the song path.
     * @return The song path.
     */
    public Path getSongPath() {
        return songPath;
    }

    /**
     * Retrieve the track part identifier.
     * @return The track part identifier.
     */
    public int getTrackPartId() {
        return trackPartId;
    }

    /**
     * Retrieve the temporary file.
     * @return The temp file.
     */
    public File getTempFile() {
        return tempFile;
    }
}
